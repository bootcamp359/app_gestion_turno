from django.forms import model_to_dict
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from gestionTurno.models import Persona, ColaEspera, Cliente, Estado, Prioridad, TipoServicios, TipoDocumento, Empleado, \
    Usuario, Puesto, Rol, Permiso, RolPermiso


# Create your views here.
@csrf_protect
def verificar(request):
    try:
        if request.method == 'POST':
            usuario = request.POST['login']
            password = request.POST['password']
            usuario = Usuario.objects.get(nombre=usuario)
            if usuario:
                if usuario.clave == password:
                    return redirect('/inicio/')
    except:
        pass
    return render(request, 'login.html', {"error": "Usuario o contraseña incorrectos"})


@csrf_protect
def login(request):
    try:
        return render(request, 'login.html')
    except:
        pass


@csrf_protect
def inicio(request):
    try:
        class Datos:
            def __init__(self, id, numeroClasificaionTikets, nombreApellido, estado, prioridad, tipoServicio):
                self.id = id
                self.numeroClasificaionTikets = numeroClasificaionTikets
                self.nombreApellido = nombreApellido
                self.estado = estado
                self.prioridad = prioridad
                self.tipoServicio = tipoServicio

            def __str__(self):
                return self.nombreApellido

        colaEspera = ColaEspera.objects.all()
        listaDatos = []
        if colaEspera:
            for c in colaEspera:
                dic = model_to_dict(c)
                numeroClasificaionTikets = c.numero_ticket
                nombreApellido = Cliente.objects.get(id_cliente=dic['id_cliente']).razon_social
                estado = Estado.objects.get(id_estado=dic['id_estado']).estado
                prioridad = Prioridad.objects.get(id_prioridad=dic['id_prioridad']).nivel_de_prioridad
                tipoServicio = TipoServicios.objects.get(id_tipo_servicio=dic['id_tipo_servicio']).nombre_servicio
                listaDatos.append(
                    Datos(c.id_cola_espera, numeroClasificaionTikets, nombreApellido, estado, prioridad, tipoServicio))
        return render(request, 'inicio.html', {"listaDatos": listaDatos})
    except:
        return render(request, 'inicio.html', {"listaDatos": listaDatos})


def listaPersona(request):
    try:
        personas = Persona.objects.all()
        listaPersonas = []
        if personas:
            for p in personas:
                dic = model_to_dict(p)
                listaPersonas.append(dic)
        return render(request, 'personas.html', {"personas": listaPersonas})
    except:
        return render(request, 'personas.html', {"personas": listaPersonas})


@csrf_protect
def registrarPersona(request):
    try:
        sexo = ['M', 'F']
        tipo_documento = TipoDocumento.objects.all()
    except:
        pass
    return render(request, 'registrarPersona.html', {"sexo": sexo, "tipo_documentos": tipo_documento})


@csrf_protect
def guardarPersona(request):
    try:
        persona = Persona()
        persona.nombre_completo = request.POST['nombrecompleto']
        persona.sexo = request.POST['sexo']
        persona.id_tipo_documento_fk = TipoDocumento.objects.get(id_tipo_documento=request.POST['tipodocumento'])
        persona.numero_documento = request.POST['numerodocumento']
        persona.direccion = request.POST['direccion']
        persona.telefono = request.POST['telefono']
        persona.email = request.POST['email']
        persona.fecha_nacimiento = request.POST['fechanacimiento']
        persona.save()
    except:
        pass
    return redirect(listaPersona)


def editarPersona(request, id):
    try:
        persona = Persona.objects.get(id=id)
        sexo = ['M', 'F']
        tipo_documento = TipoDocumento.objects.all()
        Documento = []
        if tipo_documento:
            for t in tipo_documento:
                dic = model_to_dict(t)
                Documento.append(dic)
        return render(request, 'editarPersona.html', {"persona": persona, "sexo": sexo, "tipo_documento": Documento})
    except:
        return render(request, 'editarPersona.html', {"persona": persona, "sexo": sexo, "tipo_documento": Documento})


def guardarPersonaEditado(request):
    try:
        persona = Persona.objects.get(id=request.POST['id'])
        persona.direccion = request.POST['direccion']
        persona.telefono = request.POST['telefono']
        persona.email = request.POST['email']
        persona.save()
    except:
        pass
    return redirect(listaPersona)


def eliminarPersona(request, id):
    try:
        persona = Persona.objects.get(id=id)
        persona.delete()
        response = redirect(listaPersona)
    except:
        response = redirect(listaPersona)
    return response


def solicitarTurno(request):
    try:
        clientes = Cliente.objects.all()
        estados = Estado.objects.all()
        prioridades = Prioridad.objects.all()
        servicios = TipoServicios.objects.all()
        listaClientes = []
        listaEstados = []
        listaPrioridades = []
        listaServicios = []
        if clientes:
            for c in clientes:
                dic = model_to_dict(c)
                listaClientes.append(dic)
        if estados:
            for c in estados:
                dic = model_to_dict(c)
                listaEstados.append(dic)
        if prioridades:
            for c in prioridades:
                dic = model_to_dict(c)
                listaPrioridades.append(dic)

        if servicios:
            for c in servicios:
                dic = model_to_dict(c)
                listaServicios.append(dic)

        return render(request, 'registrarTurno.html',
                      {"clientes": listaClientes, "estados": listaEstados, "prioridades": listaPrioridades,
                       "servicios": listaServicios})
    except:
        return render(request, 'registrarTurno.html',
                      {"clientes": listaClientes, "estados": listaEstados, "prioridades": listaPrioridades,
                       "servicios": listaServicios})


@csrf_protect
def guardarTurno(request):
    try:
        print(request.POST)
        colaEspera = ColaEspera()
        colaEspera.id_cliente = Cliente.objects.get(id_cliente=request.POST['cliente'])
        colaEspera.id_estado = Estado.objects.get(id_estado=request.POST['estado'])
        colaEspera.id_prioridad = Prioridad.objects.get(id_prioridad=request.POST['prioridad'])
        colaEspera.id_tipo_servicio = TipoServicios.objects.get(id_tipo_servicio=request.POST['servicio'])
        colaEspera.numero_ticket = 'T' + request.POST['cliente'] + request.POST['estado'] + request.POST['prioridad'] + \
                                   request.POST['servicio']
        colaEspera.save()
    except:
        pass
    return redirect(inicio)


def editarTurno(request, id):
    try:
        colaEspera = ColaEspera.objects.get(id_cola_espera=id)
        estados = Estado.objects.all()
        prioridades = Prioridad.objects.all()
        listaEstados = []
        listaPrioridades = []
        if estados:
            for c in estados:
                dic = model_to_dict(c)
                listaEstados.append(dic)
        if prioridades:
            for c in prioridades:
                dic = model_to_dict(c)
                listaPrioridades.append(dic)
        return render(request, 'editarTurno.html',
                      {"colaEspera": colaEspera, "estados": listaEstados, "prioridades": listaPrioridades})
    except:
        return render(request, 'editarTurno.html',
                      {"colaEspera": colaEspera, "estados": listaEstados, "prioridades": listaPrioridades})


@csrf_protect
def guardarTurnoEditado(request):
    try:
        colaEspera = ColaEspera.objects.get(id_cola_espera=request.POST['id'])
        colaEspera.id_estado = Estado.objects.get(id_estado=request.POST['estado'])
        colaEspera.id_prioridad = Prioridad.objects.get(id_prioridad=request.POST['id_prioridad'])
        colaEspera.save()
    except:
        pass
    return redirect(inicio)


def listarEmpleados(request):
    try:
        empleados = Empleado.objects.all()
        listaEmpleados = []
        if empleados:
            for e in empleados:
                dic = model_to_dict(e)
                listaEmpleados.append(dic)
        return render(request, 'empleados.html', {"empleados": empleados})
    except:
        pass


def registrarEmpleado(request):
    try:
        puestos = Puesto.objects.all()
        usuarios = Usuario.objects.all()
        personas = Persona.objects.all()
    except:
        pass
    return render(request, 'registrarEmpleado.html',
                  {'puestos': puestos, 'usuarios': usuarios, 'personas': personas})


def guardarEmpleado(request):
    try:
        empleado = Empleado()
        empleado.id_puesto_fk = Puesto.objects.get(id_puesto=request.POST['puesto'])
        empleado.id_usuario_fk = Usuario.objects.get(id=request.POST['usuario'])
        empleado.id_persona_fk = Persona.objects.get(id=request.POST['persona'])
        empleado.save()
    except:
        pass
    return redirect(listarEmpleados)


def editarEmpleado(request, id):
    try:
        empleado = Empleado.objects.get(id_empleado=id)
        puestos = Puesto.objects.all()
        usuarios = Usuario.objects.all()
        personas = Persona.objects.all()

    except:
        pass
    return render(request, 'editarEmpleado.html',
                  {'empleado': empleado, 'puestos': puestos, 'usuarios': usuarios, 'personas': personas})


@csrf_protect
def guardarEmpleadoEditado(request):
    try:
        empleado = Empleado.objects.get(id_empleado=request.POST['id'])
        empleado.id_puesto_fk = Puesto.objects.get(id_puesto=request.POST['puesto'])
        empleado.id_usuario_fk = Usuario.objects.get(id=request.POST['usuario'])
        empleado.save()
    except:
        pass
    return redirect(listarEmpleados)


def eliminarEmpleado(request, id):
    try:
        empleado = Empleado.objects.get(id_empleado=id)
        empleado.delete()
    except:
        pass
    return redirect(listarEmpleados)


def listarClientes(request):
    try:
        clientes = Cliente.objects.all()
    except:
        pass
    return render(request, 'clientes.html', {"clientes": clientes})


def registrarCliente(request):
    try:
        personas = Persona.objects.all()
    except:
        pass
    return render(request, 'registrarCliente.html', {"personas": personas})


def guardarCliente(request):
    try:
        cliente = Cliente()
        cliente.id_persona_fk = Persona.objects.get(id=request.POST['persona'])
        cliente.ruc = request.POST['ruc']
        cliente.razon_social = request.POST['razonsocial']
        cliente.save()
    except:
        pass
    return redirect(listarClientes)


def editarCliente(request, id):
    try:
        cliente = Cliente.objects.get(id_cliente=id)
    except:
        pass
    return render(request, 'editarCliente.html', {"cliente": cliente})


def guardarClienteEditado(request):
    try:
        cliente = Cliente.objects.get(id_cliente=request.POST['id'])
        cliente.ruc = request.POST['ruc']
        cliente.razon_social = request.POST['razonsocial']
        cliente.save()
    except:
        pass
    return redirect(listarClientes)


def eliminarCliente(request, id):
    try:
        cliente = Cliente.objects.get(id_cliente=id)
        cliente.delete()
    except:
        pass
    return redirect(listarClientes)


def listarServicios(request):
    try:
        servicios = TipoServicios.objects.all()
    except:
        pass
    return render(request, 'servicios.html', {"servicios": servicios})


def registrarServicio(request):
    try:
        return render(request, 'registrarServicio.html')
    except:
        pass


def guardarServicio(request):
    try:
        servicio = TipoServicios()
        servicio.nombre_servicio = request.POST['servicio']
        servicio.save()
    except:
        pass
    return redirect(listarServicios)


def editarServicio(request, id):
    try:
        servicio = TipoServicios.objects.get(id_tipo_servicio=id)
        return render(request, 'editarServicio.html', {"servicio": servicio})
    except:
        pass


def guardarServicioEditado(request):
    try:
        servicio = TipoServicios.objects.get(id_tipo_servicio=request.POST['id'])
        servicio.nombre_servicio = request.POST['servicio']
        servicio.save()
    except:
        pass
    return redirect(listarServicios)


def eliminarServicio(request, id):
    try:
        servicio = TipoServicios.objects.get(id_tipo_servicio=id)
        servicio.delete()
    except:
        pass
    return redirect(listarServicios)


def listarPrioridades(request):
    try:
        prioridades = Prioridad.objects.all()
    except:
        pass
    return render(request, 'prioridades.html', {"prioridades": prioridades})


def registrarPrioridad(request):
    try:
        return render(request, 'registrarPrioridad.html')
    except:
        pass


def guardarPrioridad(request):
    try:
        prioridad = Prioridad()
        prioridad.nivel_de_prioridad = request.POST['prioridad']
        prioridad.save()
    except:
        pass
    return redirect(listarPrioridades)


def editarPrioridad(request, id):
    try:
        prioridad = Prioridad.objects.get(id_prioridad=id)
        return render(request, 'editarPrioridad.html', {"prioridad": prioridad})
    except:
        pass


def guardarPrioridadEditado(request):
    try:
        prioridad = Prioridad.objects.get(id_prioridad=request.POST['id'])
        prioridad.nivel_de_prioridad = request.POST['prioridad']
        prioridad.save()
    except:
        pass
    return redirect(listarPrioridades)


def eliminarPrioridad(request, id):
    try:
        prioridad = Prioridad.objects.get(id_prioridad=id)
        prioridad.delete()
    except:
        pass
    return redirect(listarPrioridades)


def listarPuestos(request):
    try:
        puestos = Puesto.objects.all()
        return render(request, 'puestos.html', {"puestos": puestos})
    except:
        pass


def registrarPuesto(request):
    try:
        tiposervicios = TipoServicios.objects.all()
        return render(request, 'registrarPuesto.html', {'tiposervicios': tiposervicios})
    except:
        pass


def guardarPuesto(request):
    try:
        puesto = Puesto()
        puesto.nombre_puesto = request.POST['puesto']
        puesto.id_tipo_servicio_fk = TipoServicios.objects.get(id_tipo_servicio=request.POST['servicio'])
        puesto.save()
    except:
        pass
    return redirect(listarPuestos)


def editarPuesto(request, id):
    try:
        puesto = Puesto.objects.get(id_puesto=id)
        tiposervicios = TipoServicios.objects.all()
        return render(request, 'editarPuesto.html', {"puesto": puesto, 'tiposervicios': tiposervicios})
    except:
        pass


def guardarPuestoEditado(request):
    try:
        puesto = Puesto.objects.get(id_puesto=request.POST['id'])
        puesto.nombre_puesto = request.POST['puesto']
        puesto.id_tipo_servicio_fk = TipoServicios.objects.get(id_tipo_servicio=request.POST['servicio'])
        puesto.save()
    except:
        pass
    return redirect(listarPuestos)


def eliminarPuesto(request, id):
    try:
        puesto = Puesto.objects.get(id_puesto=id)
        puesto.delete()
    except:
        pass
    return redirect(listarPuestos)


def listarRoles(request):
    try:
        roles = Rol.objects.all()
        return render(request, 'roles.html', {"roles": roles})
    except:
        pass


def registrarRol(request):
    try:
        return render(request, 'registrarRol.html')
    except:
        pass


def guardarRol(request):
    try:
        rol = Rol()
        rol.codigo = request.POST['codigo']
        rol.descripcion = request.POST['rol']
        rol.save()
    except:
        pass
    return redirect(listarRoles)


def editarRol(request, id):
    try:
        rol = Rol.objects.get(id_rol=id)
        return render(request, 'editarRol.html', {"rol": rol})
    except:
        pass


def guardarRolEditado(request):
    try:
        rol = Rol.objects.get(id_rol=request.POST['id'])
        rol.codigo = request.POST['codigo']
        rol.descripcion = request.POST['rol']
        rol.save()
    except:
        pass
    return redirect(listarRoles)


def eliminarRol(request, id):
    try:
        rol = Rol.objects.get(id_rol=id)
        rol.delete()
    except:
        pass
    return redirect(listarRoles)


def listarPermisos(request):
    try:
        permisos = Permiso.objects.all()
        return render(request, 'permisos.html', {"permisos": permisos})
    except:
        pass


def registrarPermiso(request):
    try:

        return render(request, 'registrarPermiso.html')
    except:
        pass


def guardarPermiso(request):
    try:
        permiso = Permiso()
        permiso.codigo = request.POST['codigo']
        permiso.descripcion = request.POST['permiso']
        permiso.save()
    except:
        pass
    return redirect(listarPermisos)


def editarPermiso(request, id):
    try:
        permiso = Permiso.objects.get(id_permiso=id)
        return render(request, 'editarPermiso.html', {"permiso": permiso})
    except:
        pass


def guardarPermisoEditado(request):
    try:
        permiso = Permiso.objects.get(id_permiso=request.POST['id'])
        permiso.codigo = request.POST['codigo']
        permiso.descripcion = request.POST['permiso']
        permiso.save()
    except:
        pass
    return redirect(listarPermisos)


def eliminarPermiso(request, id):
    try:
        permiso = Permiso.objects.get(id_permiso=id)
        permiso.delete()
    except:
        pass
    return redirect(listarPermisos)


def listarRolesPermisos(request):
    try:
        permisosroles = RolPermiso.objects.all()
        return render(request, 'permisosroles.html', {"permisosroles": permisosroles})
    except:
        pass


def registrarRolPermiso(request):
    try:
        roles = Rol.objects.all()
        permisos = Permiso.objects.all()
        return render(request, 'registrarRolPermiso.html', {'roles': roles, 'permisos': permisos})
    except:
        pass


def guardarRolPermiso(request):
    rolpermiso = RolPermiso()
    rolpermiso.id_rol_fk = Rol.objects.get(id_rol=request.POST['rol'])
    rolpermiso.id_permiso_fk = Permiso.objects.get(id_permiso=request.POST['permiso'])
    rolpermiso.save()
    return redirect(listarRolesPermisos)


def editarRolPermiso(request, id):
    try:
        rolpermiso = RolPermiso.objects.get(id_rol_permiso=id)
        roles = Rol.objects.all()
        permisos = Permiso.objects.all()
        return render(request, 'editarRolPermiso.html',
                      {"rolpermiso": rolpermiso, 'roles': roles, 'permisos': permisos})
    except:
        pass


def guardarRolPermisoEditado(request):
        rolpermiso = RolPermiso.objects.get(id_rol_permiso=request.POST['id'])
        rolpermiso.id_rol_fk = Rol.objects.get(id_rol=request.POST['rol'])
        rolpermiso.id_permiso_fk = Permiso.objects.get(id_permiso=request.POST['permiso'])
        rolpermiso.save()

        return redirect(listarRolesPermisos)


def eliminarRolPermiso(request, id):
    try:
        rolpermiso = RolPermiso.objects.get(id_rol_permiso=id)
        rolpermiso.delete()
    except:
        pass
    return redirect(listarRolesPermisos)



def listarTipoDocumentos(request):
    try:
        tipodocumentos = TipoDocumento.objects.all()
        return render(request, 'tipodocumentos.html', {"tipodocumentos": tipodocumentos})
    except:
        pass

def registrarTipoDocumento(request):
    try:
        return render(request, 'registrarTipoDocumento.html')
    except:
        pass

def guardarTipoDocumento(request):
    try:
        tipodocumento = TipoDocumento()
        tipodocumento.descripcion = request.POST['descripcion']
        tipodocumento.activo = request.POST['activo']
        tipodocumento.fecha_modificacion=request.POST['fechamodificacion']
        tipodocumento.usuario_modificacion=request.POST['usuariomodificacion']
        tipodocumento.save()
    except:
        pass
    return redirect(listarTipoDocumentos)


def editarTipoDocumento(request, id):
    try:
        tipodocumento = TipoDocumento.objects.get(id_tipo_documento=id)
        return render(request, 'editarTipoDocumento.html', {"tipodocumento": tipodocumento})
    except:
        pass
def guardarTipoDocumentoEditado(request):
    try:
        tipodocumento = TipoDocumento.objects.get(id_tipo_documento=request.POST['id'])
        tipodocumento.descripcion = request.POST['descripcion']
        tipodocumento.activo = request.POST['activo']
        tipodocumento.fecha_modificacion=request.POST['fechamodificacion']
        tipodocumento.usuario_modificacion=request.POST['usuariomodificacion']
        tipodocumento.save()
    except:
        pass
    return redirect(listarTipoDocumentos)


def eliminarTipoDocumento(request, id):
    try:
        tipodocumento = TipoDocumento.objects.get(id_tipo_documento=id)
        tipodocumento.delete()
    except:
        pass
    return redirect(listarTipoDocumentos)
