from django.contrib import admin

from gestionTurno.models import Empleado, Cliente, Persona, Puesto, Permiso, Usuario, TipoServicios, Prioridad, \
    ColaEspera, Estado, Rol, RolPermiso, TipoDocumento

# Register your models here.
class EmpleadoAdmin(admin.ModelAdmin):
    list_display = ('id_empleado', 'id_empleado', 'id_empleado', 'id_empleado')
    search_fields = ('id_empleado', 'id_empleado', 'id_empleado', 'id_empleado')
    list_filter = ('id_empleado', 'id_empleado', 'id_empleado', 'id_empleado')
admin.site.register(Empleado, EmpleadoAdmin)


admin.site.register(Cliente)
admin.site.register(Persona)
admin.site.register(Puesto)
admin.site.register(Permiso)
admin.site.register(Usuario)
admin.site.register(TipoServicios)
admin.site.register(Prioridad)
admin.site.register(ColaEspera)
admin.site.register(Estado)
admin.site.register(Rol)
admin.site.register(RolPermiso)
admin.site.register(TipoDocumento)

