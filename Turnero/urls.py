"""Turnero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.shortcuts import redirect
from django.contrib import admin
from django.urls import path

from gestionTurno.views import inicio, registrarPersona, solicitarTurno, guardarPersona, guardarTurno, listaPersona, \
    editarPersona, guardarPersonaEditado, eliminarPersona, editarTurno, guardarTurnoEditado, listarEmpleados, \
    registrarEmpleado, guardarEmpleado, editarEmpleado, guardarEmpleadoEditado, eliminarEmpleado, listarClientes, \
    registrarCliente, guardarCliente, eliminarCliente, editarCliente, guardarClienteEditado, listarServicios, \
    registrarServicio, guardarServicio, editarServicio, guardarServicioEditado, eliminarServicio, listarPrioridades, \
    registrarPrioridad, guardarPrioridad, eliminarPrioridad, editarPrioridad, guardarPrioridadEditado, listarPuestos, \
    registrarPuesto, guardarPuesto, editarPuesto, guardarPuestoEditado, eliminarPuesto, login, verificar, listarRoles, \
    registrarRol, guardarRol, editarRol, guardarRolEditado, eliminarRol, listarPermisos, registrarPermiso, \
    guardarPermiso, editarPermiso, guardarPermisoEditado, eliminarPermiso, listarRolesPermisos, registrarRolPermiso, \
    guardarRolPermiso, editarRolPermiso, guardarRolPermisoEditado, eliminarRolPermiso, listarTipoDocumentos, \
    registrarTipoDocumento, guardarTipoDocumento, editarTipoDocumento, guardarTipoDocumentoEditado, \
    eliminarTipoDocumento

urlpatterns = [
    path('admin/', admin.site.urls),
    path('inicio/', inicio),
    path('', lambda req: redirect('login/')),
    path('logout/',login),
    path('login/',login),
    path('verificar/',verificar),


    path('solicitarturno/', solicitarTurno),
    path('guardarturno/', guardarTurno),
    path('editarturno/<int:id>/', editarTurno),
    path('guardarturnoeditado/', guardarTurnoEditado),

    path('persona/', listaPersona),
    path('registrarpersona/', registrarPersona),
    path('guardarpersona/', guardarPersona),
    path('editarpersona/<int:id>/', editarPersona),
    path('guardarpersonaeditado/', guardarPersonaEditado),
    path('eliminarpersona/<int:id>/', eliminarPersona),

    path('empleados/',listarEmpleados),
    path('registrarempleado/',registrarEmpleado),
    path('guardarempleado/',guardarEmpleado),
    path('editarempleado/<int:id>/',editarEmpleado),
    path('guardarempleadoeditado/',guardarEmpleadoEditado),
    path('eliminarempleado/<int:id>/',eliminarEmpleado),

    path('clientes/',listarClientes),
    path('registrarcliente/',registrarCliente),
    path('guardarcliente/',guardarCliente),
    path('editarcliente/<int:id>/',editarCliente),
    path('guardarclienteeditado/',guardarClienteEditado),
    path('eliminarcliente/<int:id>/',eliminarCliente),


    path('servicios/',listarServicios),
    path('registrarservicio/',registrarServicio),
    path('guardarservicio/',guardarServicio),
    path('editarservicio/<int:id>/',editarServicio),
    path('guardarservicioeditado/',guardarServicioEditado),
    path('eliminarservicio/<int:id>/',eliminarServicio),

    path('prioridades/',listarPrioridades),
    path('registrarprioridad/',registrarPrioridad),
    path('guardarprioridad/',guardarPrioridad),
    path('editarprioridad/<int:id>/',editarPrioridad),
    path('guardarprioridadeditado/',guardarPrioridadEditado),
    path('eliminarprioridad/<int:id>/',eliminarPrioridad),

    path('puestos/',listarPuestos),
    path('registrarpuesto/',registrarPuesto),
    path('guardarpuesto/',guardarPuesto),
    path('editarpuesto/<int:id>/',editarPuesto),
    path('guardarpuestoeditado/',guardarPuestoEditado),
    path('eliminarpuesto/<int:id>/',eliminarPuesto),

    path('roles/',listarRoles),
    path('registrarrol/',registrarRol),
    path('guardarrol/',guardarRol),
    path('editarrol/<int:id>/',editarRol),
    path('guardarroleditado/',guardarRolEditado),
    path('eliminarrol/<int:id>/',eliminarRol),

    path('permisos/',listarPermisos),
    path('registrarpermiso/',registrarPermiso),
    path('guardarpermiso/',guardarPermiso),
    path('editarpermiso/<int:id>/',editarPermiso),
    path('guardarpermisoeditado/',guardarPermisoEditado),
    path('eliminarpermiso/<int:id>/',eliminarPermiso),


    path('rolespermisos/',listarRolesPermisos),
    path('registrarrolpermiso/',registrarRolPermiso),
    path('guardarrolpermiso/',guardarRolPermiso),
    path('editarrolpermiso/<int:id>/',editarRolPermiso),
    path('guardarrolpermisoeditado/',guardarRolPermisoEditado),
    path('eliminarrolpermiso/<int:id>/',eliminarRolPermiso),

    path('tipodocumentos/',listarTipoDocumentos),
    path('registrartipodocumento/',registrarTipoDocumento),
    path('guardartipodocumento/',guardarTipoDocumento),
    path('editartipodocumento/<int:id>/',editarTipoDocumento),
    path('guardartipodocumentoeditado/',guardarTipoDocumentoEditado),
    path('eliminartipodocumento/<int:id>/',eliminarTipoDocumento),


]
