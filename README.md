#   App para administrar turnos realizado como proyecto en el bootcamp FPUNA

## Clonar el repositorio
- git clone git@github.com:LuisAlbertoCB/App_Gestion_Turno.git
- cd App_Gestion_Turno
## Crear un entorno virtual de python e activar

### En Windows Ejecutar
- python -m venv env
- env\Scripts\activate.bat
## Instalar requerimientos
- pip install -r requirements.txt
## Crear la base de datos
- Agregar los datos de la base de datos en el archivo `Turnero/settings.py`
- python manage.py migrate
- python manage.py makemigrations
- python manage.py migrate gestionTurno
- - python manage.py makemigrations gestionTurno
## Iniciar el servidor
- python manage.py runserver
- Abrir el navegador en la direccion `http://127.0.0.1:8000`

