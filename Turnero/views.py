
from django.http import HttpResponse
from django.shortcuts import render
from django.template import Template, Context, loader
from django.utils.datetime_safe import datetime
from gestionTurno.models import Persona


def saludo(request):
    nombre="Luis"
    apellido="Baez"
    temas=["Plantillas","Modelos","Formularios","Vistas","Despliegue"].clear()
    fecha_actual=datetime.now()
    doc_externo=open("templates/plantilla.html");
    plt= Template(doc_externo.read());
    doc_externo.close();

    print(Persona.objects.get(id=1).nombre)
    ctx=Context({"nombre_persona":nombre, "apellido_persona":apellido, "fecha":fecha_actual,"temas":temas});
    documento=plt.render(ctx);
    return HttpResponse(documento);


def saludo2(request):
    nombre = "Luis"
    apellido = "Baez"
    temas = ["Plantillas", "Modelos", "Formularios", "Vistas", "Despliegue"].clear()
    fecha_actual = datetime.now()
    doc_externo=loader.get_template('plantilla.html')
    documento=doc_externo.render({"nombre_persona":nombre, "apellido_persona":apellido, "fecha":fecha_actual,"temas":temas});
    return HttpResponse(documento);

def saludo3(request):
    nombre = "Luis"
    apellido = "Baez"
    temas = ["Plantillas", "Modelos", "Formularios", "Vistas", "Despliegue"].clear()
    fecha_actual = datetime.now()

    return render(request, 'plantilla.html', {"nombre_persona":nombre, "apellido_persona":apellido, "fecha":fecha_actual,"temas":temas});
