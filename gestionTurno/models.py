# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    persona_id_fk = models.ForeignKey('Persona', on_delete=models.CASCADE, db_column='persona_id_fk')
    ruc = models.CharField(max_length=20, blank=True, null=True)
    razon_social = models.CharField(max_length=128)

    class Meta:
        db_table = 'cliente'


class ColaEspera(models.Model):
    id_cola_espera = models.AutoField(primary_key=True)
    id_cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, db_column='id_cliente')
    id_estado = models.ForeignKey('Estado', on_delete=models.CASCADE, db_column='id_estado')
    id_prioridad = models.ForeignKey('Prioridad', on_delete=models.CASCADE, db_column='id_prioridad')
    id_tipo_servicio = models.ForeignKey('TipoServicios', on_delete=models.CASCADE, db_column='id_tipo_servicio')
    numero_ticket = models.CharField(max_length=10)

    class Meta:
        db_table = 'cola_espera'


class Empleado(models.Model):
    id_empleado = models.AutoField(primary_key=True)
    id_puesto_fk = models.ForeignKey('Puesto', on_delete=models.CASCADE, db_column='id_puesto_fk')
    id_usuario_fk = models.ForeignKey('Usuario',on_delete=models.CASCADE, db_column='id_usuario_fk')
    id_persona_fk = models.ForeignKey('Persona', on_delete=models.CASCADE, db_column='id_persona_fk')

    class Meta:
        db_table = 'empleado'

ESTADOS_CHOICES = [
    ('PENDIENTE','PENDIENTE'),
    ('EN CURSO','EN CURSO'),
    ('FINALIZADO','FINALIZADO'),
    ('CANCELADO','CANCELADO'),
]
class Estado(models.Model):
    id_estado = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=15, choices=ESTADOS_CHOICES)

    class Meta:
        db_table = 'estado'


class Permiso(models.Model):
    id_permiso = models.SmallAutoField(primary_key=True)
    codigo = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=32)

    class Meta:
        db_table = 'permiso'

SEXO_CHOICES = [
    ('M','M'),
    ('F','F'),
]
class Persona(models.Model):
    id_tipo_documento_fk = models.ForeignKey('TipoDocumento', on_delete=models.CASCADE, db_column='id_tipo_documento_fk')
    numero_documento = models.CharField(unique=True, max_length=16)
    nombre_completo = models.CharField(max_length=64)
    fecha_nacimiento = models.DateField()
    direccion = models.CharField(max_length=70, blank=True, null=True)
    telefono = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    sexo = models.CharField(max_length=1, choices=SEXO_CHOICES)

    class Meta:
        db_table = 'persona'

PRIORIDAD_CHOICES = [
    (1,'BAJA'),
    (2,'MEDIA'),
    (3,'ALTA'),
]
class Prioridad(models.Model):
    id_prioridad = models.AutoField(primary_key=True)
    nivel_de_prioridad = models.IntegerField(choices=PRIORIDAD_CHOICES)

    class Meta:
        db_table = 'prioridad'


class Puesto(models.Model):
    id_puesto = models.AutoField(primary_key=True)
    nombre_puesto = models.CharField(max_length=20)
    id_tipo_servicio_fk = models.ForeignKey('TipoServicios', on_delete=models.CASCADE, db_column='id_tipo_servicio_fk')

    class Meta:
        db_table = 'puesto'


class Rol(models.Model):
    id_rol = models.SmallAutoField(primary_key=True)
    codigo = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=32)

    class Meta:
        db_table = 'rol'


class RolPermiso(models.Model):
    id_rol_permiso = models.SmallAutoField(primary_key=True)
    id_permiso_fk = models.ForeignKey(Permiso, on_delete=models.CASCADE, db_column='id_permiso_fk')
    id_rol_fk = models.ForeignKey(Rol, on_delete=models.CASCADE, db_column='id_rol_fk')

    class Meta:
        db_table = 'rol_permiso'

TIPO_DOCUMENTO_CHOICES = [
    ('CI','CI'),
    ('RUC','RUC'),
]
class TipoDocumento(models.Model):
    id_tipo_documento = models.SmallAutoField(primary_key=True)
    descripcion = models.CharField(max_length=16, choices=TIPO_DOCUMENTO_CHOICES)
    activo = models.BooleanField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        db_table = 'tipo_documento'


class TipoServicios(models.Model):
    id_tipo_servicio = models.AutoField(primary_key=True)
    nombre_servicio = models.CharField(max_length=20)

    class Meta:
        db_table = 'tipo_servicios'


class Usuario(models.Model):
    nombre = models.CharField(unique=True, max_length=16)
    id_rol_permiso_fk = models.ForeignKey(RolPermiso, on_delete=models.CASCADE, db_column='id_rol_permiso_fk')
    clave = models.CharField(max_length=32)
    activo = models.BooleanField()

    class Meta:
        db_table = 'usuario'
